<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Fiji Secondary Schools Educational Resources</title>
    <!-- Basic Styles -->
	<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="js/DataTables/datatables.css">
	<link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
	<!-- SmartAdmin Styles : Caution! DO NOT change the order -->

	<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-production.min.css">
	<link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
	
</head>
<body class="">
	<!-- HEADER -->
	<header id="header">
		<div>
			
				<img  id="zavouit-logo" class="logo" style="height:45px" src="img/zavou_logo_it.png" alt="ZAVOU IT Logo Fiji">
			
		</div>
		<div>
		<h2> Fiji Secondary Schools Educational Resources</h2>
	    </div>

	</header>
	<!-- END HEADER -->

	<!-- Left panel : Navigation area -->
	<!-- Note: This width of the aside area can be adjusted through LESS variables -->
	<aside id="left-panel">

		<!-- User info -->
		<div class="login-info">
			<span>
				
				

			</span>
		</div>
		<!-- end user info -->

		<!-- NAVIGATION : This navigation is also responsive-->
		<nav>
			<!-- 
				NOTE: Notice the gaps after each icon usage <i></i>..
				Please note that these links work a bit different than
				traditional href="" links. See documentation for details.
				-->

			<ul>
				<li class="active">
					<a href="#" title="Dashboard"><i class="fa fa-lg fa-fw fa-file"></i> <span class="menu-item-parent">Past Exam
							Papers</span></a>
					<ul>
						<li class="">
							<a href="paper/index?year=9" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-file"></i><span
								 class="menu-item-parent">Year
									9</span></a>
						</li>
						<li class="">
							<a href="paper/index?year=10" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-file"></i><span
								 class="menu-item-parent">Year
									10</span></a>
						</li>
						<li class="">
							<a href="paper/index?year=11" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-file"></i><span class="menu-item-parent">Year
									11</span></a>
						</li>
						<li class="">
							<a href="paper/index?year=12" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-file"></i><span class="menu-item-parent">Year
									12</span></a>
						</li>
						<li class="">
							<a href="paper/index?year=13" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-file"></i><span class="menu-item-parent">Year
									13</span></a>
						</li>

					</ul>
				</li>
				<li class="top-menu-invisible">
					<a href="#" title="Dashboard"><i class="fa fa-lg fa-fw fa-book"></i> <span class="menu-item-parent">Text Books</span></a>
					<ul>
						<li class="">
							<a href="textbooks.php?year=9" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-calendar-alt"></i><span class="menu-item-parent">Year
									9</span></a>
						</li>
						<li class="">
							<a href="textbooks.php?year=10" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-calendar-alt"></i><span class="menu-item-parent">Year
									10</span></a>
						</li>
						<li class="">
							<a href="textbooks.php?year=11" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-calendar-alt"></i><span class="menu-item-parent">Year
									11</span></a>
						</li>
						<li class="">
							<a href="textbooks.php?year=12" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-calendar-alt"></i><span class="menu-item-parent">Year
									12</span></a>
						</li>
						<li class="">
							<a href="textbooks.php?year=13" onclick="doalert(this); return false;" title="Dashboard"><i class="fa fa-lg fa-fw fa-calendar-alt"></i><span class="menu-item-parent">Year
									13</span></a>
						</li>

					</ul>
				</li>

				<li class="top-menu-invisible">
					<a href="#" title="Dashboard"><i class="fa fa-lg fa-fw fa-wrench"></i> <span class="menu-item-parent">Extra
							Resouces</span></a>
					<ul>
						<li class="">
							<a href="#" title="Dashboard"><i class="fa fa-lg fa-fw fa-user-graduate"></i><span class="menu-item-parent">Students</span></a>
						</li>
						<li class="">
							<a href="#" title="Dashboard"><i class="fa fa-lg fa-fw fa-chalkboard-teacher"></i><span class="menu-item-parent">Teachers</span></a>
						</li>

					</ul>
				</li>
			</ul>
		</nav>


		<span class="minifyme" data-action="minifyMenu">
			<i class="fa fa-arrow-circle-left hit"></i>
		</span>

	</aside>
	<!-- END NAVIGATION -->

	<!-- MAIN PANEL -->
	<div id="main" role="main">

		<!-- RIBBON -->
		<div id="ribbon">

			<span class="ribbon-button-alignment">
				<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip"
				 data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings."
				 data-html="true">
					<i class="fa fa-refresh"></i>
				</span>
			</span>

			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Dashboard</li>
			</ol>
			<!-- end breadcrumb -->

			<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">

			<!--ENTER MAIN CONTENT-->



		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<!-- END MAIN PANEL -->

	<!-- PAGE FOOTER -->
	<div class="page-footer">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<span class="txt-color-white"><a href="https://www.zavouit.com" target="_blank" style="color:#fff;text-decoration-line: underline;">ZAVOU IT ©
					2018</a>
			</div>

			<div class="col-xs-6 col-sm-6 text-right hidden-xs">
				
			</div>
		</div>
	</div>
	<!-- END PAGE FOOTER -->

	<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
		Note: These tiles are completely responsive,
		you can add as many as you like
		-->
	<div id="shortcut">
		<ul>
			<li>
				<a href="inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i>
						<span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
			</li>
			<li>
				<a href="calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i>
						<span>Calendar</span> </span> </a>
			</li>
			<li>
				<a href="gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i>
						<span>Maps</span> </span> </a>
			</li>
			<li>
				<a href="invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i>
						<span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
			</li>
			<li>
				<a href="gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i>
						<span>Gallery </span> </span> </a>
			</li>
			<li>
				<a href="profile.html" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i
						 class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
			</li>
		</ul>
	</div>
	<!-- END SHORTCUT AREA -->

	<!--================================================== -->


	<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->

	<script src="js/libs/jquery-3.2.1.min.js"></script>

	<!-- IMPORTANT: APP CONFIG -->
	<script src="js/app.config.js"></script>

	<script src="js/DataTables/datatables.js"></script>

	<!-- MAIN APP JS FILE -->
	<script src="js/app.min.js"></script>
	<script>
		function doalert(obj) {
			url = obj.getAttribute("href")
            //mainUrl = "http://localhost/secondary/web/site/index"
            //alert(url);
			$.get(url, function (data, status) {
				jQuery("#content").html(data);
				$('#test_papers').DataTable();
			});
			return false;

		}
		$("li").click(function () {
			$(this).toggleClass("active");
			$(this).siblings().removeClass("active");
		});
	</script>


</body>

</html>

